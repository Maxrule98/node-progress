const express = require('express');
const app = express();

const fs = require('fs');

const port = 3000;

app.set('view engine', 'ejs');

/*---------------------------------------------*/

var peopleData = require('./data.json');

/*---------------------------------------------*/

app.get('/', (req,res) => {
    res.render('index.ejs', {
        title: 'Cool People',
        subtitle: 'Loaded Dynamically!',
        people: peopleData
    });
});

/*---------------------------------------------*/

app.listen(port, () => {
    console.log('Server is started on: http://localhost:' + port)
});